package org.insa.graph;

//---a verifier
import static org.junit.Assert.assertEquals;
import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import org.insa.algo.ArcInspectorFactory;
import org.insa.algo.shortestpath.BellmanFordAlgorithm;
import org.insa.algo.shortestpath.AStarAlgorithm;
import org.insa.algo.shortestpath.ShortestPathData;
import org.insa.algo.shortestpath.ShortestPathSolution;
import org.insa.graph.io.BinaryGraphReader;
import org.insa.graph.io.GraphReader;
import org.junit.Test;
//----

public class AstarTest {

    //Test de validit� avec oracle sur une carte
    @Test
    public void testShortestPathMap() throws IOException {
    	System.out.println(" Test : sur chemins al�atoires");
       	String mapName = "map/haute-garonne.mapgr";
        GraphReader reader = new BinaryGraphReader(new DataInputStream(new BufferedInputStream(new FileInputStream(mapName))));
        Graph graph = reader.read();


        int count, p1, p2;
        int size = graph.size();
        int end =5;
        ShortestPathSolution AStarPath, BellmanPath;
		
		for(count = 0; count <end; count++) {
			p1 = (int) (Math.random()*size);
        	p2 = (int) (Math.random()*size);
			
    		ShortestPathData data = new ShortestPathData(graph, graph.get(p1), graph.get(p2), ArcInspectorFactory.getAllFilters().get(count));
    		
    		AStarAlgorithm AStar = new AStarAlgorithm(data);
    		BellmanFordAlgorithm bellman = new BellmanFordAlgorithm(data);
    		AStarPath = AStar.run();
    		BellmanPath = bellman.run();
    		
    		if(BellmanPath.isFeasible()) {
    			assertEquals(BellmanPath.getPath().getLength(),AStarPath.getPath().getLength(),0.01);
    			switch(count) {
    			case 0:
    				System.out.println("Mode :Length, all roads");
    				break;
    			case 1:
    				System.out.println("Mode :Length, only roads for cars");
    				break;
    			case 2:
    				System.out.println("Mode :Time, all roads");
    				break;
    			case 3:
    				System.out.println("Mode :Time, only roads for cars");
    				break;
    			case 4:
    				System.out.println("Mode :Time, pedestrian");
    				break;
    				
    			default : break;
    			}
    			System.out.println(count+") TEMPS | AStar : "+AStarPath.getPath().getMinimumTravelTime()+" / Bellman : "+BellmanPath.getPath().getMinimumTravelTime());
    			System.out.println(count+") DISTANCE | AStar : "+AStarPath.getPath().getLength()+" / Bellman : "+BellmanPath.getPath().getLength());
    			System.out.println(count+") TEMPS D'EXECUTION | AStar : "+AStarPath.getSolvingTime().getNano()+"ns / Bellman : "+BellmanPath.getSolvingTime().getSeconds()+"s");
    			System.out.println("-------------------------------------------------------------");
    			System.out.println("");
    		}
    		
    		else {
    			assertEquals(BellmanPath.isFeasible(),AStarPath.isFeasible());
    			System.out.println(count+") AStar & Bellman infaisable");
    		}
    		
		}
	}
    //Test de validit� sans oracle sur une carte
      @Test
    public void testNoPathMap() throws IOException {
    	  System.out.println("Test : chemin impossible");

       	String mapName = "map/new-caledonia.mapgr";
        GraphReader reader = new BinaryGraphReader(new DataInputStream(new BufferedInputStream(new FileInputStream(mapName))));
        Graph graph = reader.read();


        int p1, p2;
        ShortestPathSolution AStarPath, BellmanPath;
		p1 = 7657;
    	p2 = 8309;
		
		ShortestPathData data = new ShortestPathData(graph, graph.get(p1), graph.get(p2), ArcInspectorFactory.getAllFilters().get(0));
		
		AStarAlgorithm AStar = new AStarAlgorithm(data);
		BellmanFordAlgorithm bellman = new BellmanFordAlgorithm(data);
		AStarPath = AStar.run();
		BellmanPath = bellman.run();
		
		if(BellmanPath.isFeasible()) {
			System.out.println(" Erreur : chemin trouv� alors que le chemin est infaisable");
		}
		
		else {
			assertEquals(BellmanPath.isFeasible(),AStarPath.isFeasible());
			System.out.println("Test r�ussi : AStar & Bellman infaisable");
			System.out.println("-------------------------------------------------------------");
		}
    		
	}
        
}

  