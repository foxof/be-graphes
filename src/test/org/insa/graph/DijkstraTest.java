package org.insa.graph;

//---a verifier
import static org.junit.Assert.assertEquals;
import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Arrays;
import org.insa.algo.ArcInspectorFactory;
import org.insa.algo.shortestpath.BellmanFordAlgorithm;
import org.insa.algo.shortestpath.DijkstraAlgorithm;
import org.insa.algo.shortestpath.ShortestPathData;
import org.insa.algo.shortestpath.ShortestPathSolution;
import org.insa.graph.RoadInformation.RoadType;
import org.insa.graph.io.BinaryGraphReader;
import org.insa.graph.io.GraphReader;
import org.junit.BeforeClass;
import org.junit.Test;
//----

public class DijkstraTest {

    // Small graph use for tests
    private static Graph graph;

    // List of nodes
    private static Node[] nodes;

    // List of arcs in the graph, x1tox2 is the arc from node x1 (0) to x2 (1).
    @SuppressWarnings("unused")
    private static Arc x1tox2,x1tox3,x3tox1,x3tox2,x3tox6,x2tox4,x2tox5,x2tox6,x6tox5,x5tox3,x5tox6,x5tox4;

    // Some paths...

    @BeforeClass
    public static void initAll() throws IOException {

        // 10 and 20 meters per seconds
        RoadInformation speed10 = new RoadInformation(RoadType.MOTORWAY, null, true, 36, "");
         

        // Create nodes
        nodes = new Node[6];
        for (int i = 0; i < nodes.length; ++i) {
            nodes[i] = new Node(i, null);
        }

        // Add arcs...
        x1tox2 = Node.linkNodes(nodes[0], nodes[1], 7, speed10, null);
        x1tox3 = Node.linkNodes(nodes[0], nodes[2], 8, speed10, null);
        x2tox4 = Node.linkNodes(nodes[1], nodes[3], 4, speed10, null);
        x2tox5 = Node.linkNodes(nodes[1], nodes[4], 1, speed10, null);
        x2tox6 = Node.linkNodes(nodes[1], nodes[5], 5, speed10, null);
        x3tox1 = Node.linkNodes(nodes[2], nodes[0], 7, speed10, null);
        x3tox2 = Node.linkNodes(nodes[2], nodes[1], 2, speed10, null);
        x3tox6 = Node.linkNodes(nodes[2], nodes[5], 2, speed10, null);
        x5tox3 = Node.linkNodes(nodes[4], nodes[2], 2, speed10, null);
        x5tox4 = Node.linkNodes(nodes[4], nodes[3], 2, speed10, null);
        x5tox6 = Node.linkNodes(nodes[4], nodes[5], 3, speed10, null);
        x6tox5 = Node.linkNodes(nodes[5], nodes[4], 3, speed10, null);

        graph = new Graph("ID", "", Arrays.asList(nodes), null);

    }


    //Affichage Tableau
    @Test
    public void testShortestPathSimpleExample() {
    	System.out.println(" Test : graph 1");
    	System.out.print("____");
    	for(int j=0; j<nodes.length; j++) {
    		System.out.print("|x"+(j+1)+" ");
    	}
		System.out.println("|");
    	
    	for(int i=0; i<nodes.length;i++) {
    		System.out.print("|x"+(i+1)+" ");
    		for(int j=0; j<nodes.length; j++) {
        		ShortestPathData data = new ShortestPathData(graph, nodes[i], nodes[j], ArcInspectorFactory.getAllFilters().get(0));
        		
        		DijkstraAlgorithm dijkstra = new DijkstraAlgorithm(data);
        		BellmanFordAlgorithm bellman = new BellmanFordAlgorithm(data);
        		
        		if(bellman.run().isFeasible()) {
        			assertEquals(bellman.run().getPath().getLength(),dijkstra.run().getPath().getLength(),0.01);
        			//System.out.println(i+" to "+j+" "+ dijkstra.run().getPath().getLength());
        				if (dijkstra.run().getPath().getLength()>=10.0) {
        					System.out.print("|"+(int)(dijkstra.run().getPath().getLength())+" ");
        				}
        				else System.out.print("| "+ (int)dijkstra.run().getPath().getLength()+" ");
        		}
        		else {
        			if(i==j) {
        				System.out.print("|0  ");
        			}
        			else {
        			assertEquals(bellman.run().isFeasible(),dijkstra.run().isFeasible());
        			System.out.print("| x ");
        			}
        		}
    		}
    		System.out.println("|");
    	}
    	System.out.println("-------------------------------------------------------------");
    	System.out.println("");
    }
    //Test de validit� avec oracle sur une carte
    @Test
    public void testShortestPathMap() throws IOException {
    	System.out.println(" Test : sur chemins al�atoires");
       	String mapName = "map/haute-garonne.mapgr";
        GraphReader reader = new BinaryGraphReader(new DataInputStream(new BufferedInputStream(new FileInputStream(mapName))));
        Graph graph = reader.read();


        int count, p1, p2;
        int size = graph.size();
        int end =5;
        ShortestPathSolution DijkstraPath, BellmanPath;
		
		for(count = 0; count <end; count++) {
			p1 = (int) (Math.random()*size);
        	p2 = (int) (Math.random()*size);
			
    		ShortestPathData data = new ShortestPathData(graph, graph.get(p1), graph.get(p2), ArcInspectorFactory.getAllFilters().get(count));
    		
    		DijkstraAlgorithm dijkstra = new DijkstraAlgorithm(data);
    		BellmanFordAlgorithm bellman = new BellmanFordAlgorithm(data);
    		DijkstraPath = dijkstra.run();
    		BellmanPath = bellman.run();
    		
    		if(BellmanPath.isFeasible()) {
    			assertEquals(BellmanPath.getPath().getLength(),DijkstraPath.getPath().getLength(),0.01);
    			switch(count) {
    			case 0:
    				System.out.println("Mode :Length, all roads");
    				break;
    			case 1:
    				System.out.println("Mode :Length, only roads for cars");
    				break;
    			case 2:
    				System.out.println("Mode :Time, all roads");
    				break;
    			case 3:
    				System.out.println("Mode :Time, only roads for cars");
    				break;
    			case 4:
    				System.out.println("Mode :Time, pedestrian");
    				break;
    				
    			default : break;
    			}
    			System.out.println(count+") TEMPS | Dijkstra : "+DijkstraPath.getPath().getMinimumTravelTime()+" / Bellman : "+BellmanPath.getPath().getMinimumTravelTime());
    			System.out.println(count+") DISTANCE | Dijkstra : "+DijkstraPath.getPath().getLength()+" / Bellman : "+BellmanPath.getPath().getLength());
    			System.out.println(count+") TEMPS D'EXECUTION | Dijkstra : "+DijkstraPath.getSolvingTime().getNano()+"ns / Bellman : "+BellmanPath.getSolvingTime().getSeconds()+"s");
    			System.out.println("-------------------------------------------------------------");
    			System.out.println("");
    		}
    		
    		else {
    			assertEquals(BellmanPath.isFeasible(),DijkstraPath.isFeasible());
    			System.out.println(count+") Dijkstra & Bellman infaisable");
    		}
    		
		}
	}
    //Test de validit� sans oracle sur une carte
      @Test
    public void testNoPathMap() throws IOException {
    	  System.out.println("Test : chemin impossible");

       	String mapName = "map/new-caledonia.mapgr";
        GraphReader reader = new BinaryGraphReader(new DataInputStream(new BufferedInputStream(new FileInputStream(mapName))));
        Graph graph = reader.read();


        int p1, p2;
        ShortestPathSolution DijkstraPath, BellmanPath;
		p1 = 7657;
    	p2 = 8309;
		
		ShortestPathData data = new ShortestPathData(graph, graph.get(p1), graph.get(p2), ArcInspectorFactory.getAllFilters().get(0));
		
		DijkstraAlgorithm dijkstra = new DijkstraAlgorithm(data);
		BellmanFordAlgorithm bellman = new BellmanFordAlgorithm(data);
		DijkstraPath = dijkstra.run();
		BellmanPath = bellman.run();
		
		if(BellmanPath.isFeasible()) {
			System.out.println(" Erreur : chemin trouv� alors que le chemin est infaisable");
		}
		
		else {
			assertEquals(BellmanPath.isFeasible(),DijkstraPath.isFeasible());
			System.out.println("Test r�ussi : Dijkstra & Bellman infaisable");
			System.out.println("-------------------------------------------------------------");
		}
    		
	}
        
}

  