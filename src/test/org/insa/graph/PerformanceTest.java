package org.insa.graph;



import java.nio.file.Paths;
import java.time.Duration;
import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;

import org.insa.algo.ArcInspector;
import org.insa.algo.ArcInspectorFactory;
import org.insa.algo.shortestpath.AStarAlgorithm;
import org.insa.algo.shortestpath.DijkstraAlgorithm;
import org.insa.algo.shortestpath.ShortestPathData;
import org.insa.graph.io.BinaryGraphReader;
import org.insa.graph.io.GraphReader;
import org.junit.Test;


public class PerformanceTest {
	
	@Test
	public void testPerfInLength() throws IOException {
		Path filePath = Paths.get("tests.csv");
		ArrayList<String> lines = (ArrayList<String>) Files.readAllLines(filePath);

		String[] temp = lines.get(0).split(",");
		int nb = Integer.decode(temp[1]);
		lines.remove(0);

		GraphReader reader = new BinaryGraphReader(new DataInputStream(new BufferedInputStream(new FileInputStream(temp[0]))));
		Graph graph = reader.read();

		int count = 0;

		for(ArcInspector insp : ArcInspectorFactory.getAllFilters()) {
			count++;
			int dijkstraMoy=0, aStarMoy=0;
			for(String str : lines) {
				String [] fileData = str.split(",");
				int p1 = Integer.decode(fileData[0]);
				int p2 = Integer.decode(fileData[1]);

				ShortestPathData data = new ShortestPathData(graph, graph.get(p1), graph.get(p2), insp);

				DijkstraAlgorithm dijkstra = new DijkstraAlgorithm(data);
				AStarAlgorithm aStar = new AStarAlgorithm(data);

				Duration dijkstraTime = dijkstra.run().getSolvingTime();
				Duration aStarTime = aStar.run().getSolvingTime();   		

				long dijkstraDuration = dijkstraTime.getSeconds()*1000 + dijkstraTime.getNano()/1000000;
				long AStarDuration = aStarTime.getSeconds()*1000 + aStarTime.getNano()/1000000;

				dijkstraMoy += dijkstraDuration;
				aStarMoy += AStarDuration;
			}

			dijkstraMoy/=nb;
			aStarMoy/=nb;
			System.out.println(count+") : DijkstraM : "+dijkstraMoy+" / A*M : "+aStarMoy);
		}
	}
}