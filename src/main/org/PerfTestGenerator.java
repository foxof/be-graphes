package org;

import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;

import org.insa.graph.Graph;
import org.insa.graph.Point;
import org.insa.graph.io.BinaryGraphReader;
import org.insa.graph.io.GraphReader;


public class PerfTestGenerator {

	public static void main(String[] args) throws Exception {
		generatePerfTest("midi-pyrenees.mapgr", 100, 200000, 250000);
	}


	public static void generatePerfTest(String graphName, int nb, double distMin, double distMax) throws Exception {

		ArrayList<String> lines = new ArrayList<String>();
		GraphReader reader = new BinaryGraphReader(new DataInputStream(new BufferedInputStream(new FileInputStream(graphName))));
		Graph graph = reader.read();

		String name = graphName.split("\\\\")[graphName.split("\\\\").length-1];
		lines.add(name+","+nb);
		int size = graph.size();
		int count=0;

		while(count != nb) {
			int p1 = (int) (Math.random()*size);
			int p2 = (int) (Math.random()*size);

			double dist = Point.distance(graph.get(p1).getPoint(), graph.get(p2).getPoint());

			if(dist <= distMax && dist >= distMin) {
				lines.add(p1+","+p2+","+(int)dist);
				count++;
			}
		}
		Path filePath = Paths.get("tests.csv");
		Files.write(filePath, lines, StandardCharsets.UTF_8);
	}
}
