package org.insa.algo.shortestpath;


import org.insa.graph.Arc;
import org.insa.graph.Node;

public class AStarAlgorithm extends DijkstraAlgorithm {

    public AStarAlgorithm(ShortestPathData data) {
        super(data);
    }
     
    @Override
    protected Label[] initLabel(ShortestPathData data, int nbNodes, double cost, Arc father, Node node) {
    	LabelStar[] lb = new LabelStar[nbNodes];
    	lb[data.getOrigin().getId()]=new LabelStar(cost, father, node, data);
    	return lb;
    }
    
    @Override
    protected Label createLabel(Double distance, Arc arc, ShortestPathData data) {
    	return new LabelStar(distance, arc, arc.getDestination(), data);
    }
}

