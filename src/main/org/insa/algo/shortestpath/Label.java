package org.insa.algo.shortestpath;

import org.insa.graph.Arc;
import org.insa.graph.Node;

public class Label implements Comparable<Label> {
	
	protected double cost;
	private boolean mark;
	private Arc father;
	private Node node;
	
	public Label(double cost, Arc father, Node node) {
		this.cost = cost;
		this.mark = false;
		this.father = father;
		this.node = node;
	}

	public String toString() {
		return "Label : "+this.mark+" Cout : "+this.cost+" father : "+this.father;
	}
	
	public double getCost() {
		return this.cost;
	}

	public boolean getMark() {
		return this.mark;
	}

	public Arc getFather() {
		return this.father;
	}
	
	public Node getNode() {
		return this.node;
	}
	
	public void setCost(double cost) {
		this.cost=cost;
	}
	
	public void setMark(boolean mark) {
		this.mark=mark;
	}
	
	public void setFather(Arc father) {
		this.father=father;
	}
	
	public double getHeuristic() {
		return 0;
	}

	@Override
	public int compareTo(Label o) {
		int res =  Double.compare(this.getCost()+this.getHeuristic(), o.getCost()+o.getHeuristic());
		if(res==0) {
			res = Double.compare(this.getHeuristic(), o.getHeuristic());
		}
		return res;
	}
}
