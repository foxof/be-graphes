package org.insa.algo.shortestpath;

import java.util.ArrayList;
import java.util.Collections;

import org.insa.algo.AbstractSolution.Status;
import org.insa.algo.utils.BinaryHeap;
import org.insa.graph.Arc;
import org.insa.graph.Graph;
import org.insa.graph.Node;
import org.insa.graph.Path;

public class DijkstraAlgorithm extends ShortestPathAlgorithm {

    public DijkstraAlgorithm(ShortestPathData data) {
        super(data);
    }
    
    protected Label[] initLabel(ShortestPathData data, int nbNodes, double cost, Arc father, Node node) {
    	Label[] lb = new Label[nbNodes];
    	lb[data.getOrigin().getId()]=new Label(cost, father, node);
    	return lb;
    }
    
    protected Label createLabel(Double distance, Arc arc, ShortestPathData data) {
    	return new Label(distance, arc, arc.getDestination());
    }

    @Override
    protected ShortestPathSolution doRun() {
        ShortestPathData data = getInputData();
		Graph graph = data.getGraph();

		final int nbNodes = graph.size();
		if(nbNodes==0)return new ShortestPathSolution(data, Status.INFEASIBLE);
		if(data.getDestination().getId()==data.getOrigin().getId()) return new ShortestPathSolution(data, Status.INFEASIBLE);

		// Initialize array of labels
		Label[] lb = initLabel(data, nbNodes, 0, null, data.getOrigin());
		
		notifyOriginProcessed(data.getOrigin());
		
		BinaryHeap<Label> heap = new BinaryHeap<Label>();
		heap.insert(lb[data.getOrigin().getId()]);
		
		while(!heap.isEmpty() && (lb[data.getDestination().getId()]==null || !lb[data.getDestination().getId()].getMark())) {
			Node x = heap.deleteMin().getNode();
			lb[x.getId()].setMark(true);
			notifyNodeMarked(x);

			//System.out.println(lb[x.getId()].getCost());
			
			for(Arc arc : x) {
				if(!data.isAllowed(arc))
					continue;
				
				int destId = arc.getDestination().getId();
				
				// Retrieve weight of the arc.
				double w = data.getCost(arc);
				double newDistance = lb[x.getId()].getCost() + w;
				
				if((lb[destId]==null || (newDistance < lb[destId].getCost()) && !lb[destId].getMark())) {
					if(lb[destId]!=null) {
						//System.out.println(lb[destId] +"/ new distance : "+newDistance + " / cost : "+lb[destId].getCost());
						heap.remove(lb[destId]);
						lb[destId].setCost(newDistance);
						lb[destId].setFather(arc);
					}
					else {
						notifyNodeReached(arc.getDestination());
						lb[destId] = createLabel(newDistance, arc, data);
					}
					heap.insert(lb[destId]);
				}					
			}
		}
	
		ShortestPathSolution solution = null;

		// Destination has no predecessor, the solution is infeasible...
		if (lb[data.getDestination().getId()] == null) {
			solution = new ShortestPathSolution(data, Status.INFEASIBLE);
		} else {

			// The destination has been found, notify the observers.
			notifyDestinationReached(data.getDestination());

			// Create the path from the array of predecessors...
			ArrayList<Arc> arcs = new ArrayList<>();
			Arc arc = lb[data.getDestination().getId()].getFather();
			while (arc != null) {
				arcs.add(arc);
				arc = lb[arc.getOrigin().getId()].getFather();
			}

			// Reverse the path...
			Collections.reverse(arcs);

			// Create the final solution.
			solution = new ShortestPathSolution(data, Status.OPTIMAL, new Path(graph, arcs));
		}
		
		return solution;
    }
}
