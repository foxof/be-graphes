package org.insa.algo.shortestpath;

import org.insa.graph.Arc;
import org.insa.graph.Node;
import org.insa.graph.Point;
import org.insa.algo.AbstractInputData.Mode;

public class LabelStar extends Label {
	
	private Node dest;
	private double heuristic;

	public LabelStar(double cost, Arc father, Node node, ShortestPathData data) {
		super(cost, father, node);
		this.dest=data.getDestination();
		double tmpHeuristic = Point.distance(this.getNode().getPoint(), dest.getPoint());
		
		if(data.getMode() == Mode.TIME) {
			int tmpMaxSpeed = Math.max(data.getMaximumSpeed(),data.getGraph().getGraphInformation().getMaximumSpeed());
			tmpHeuristic = tmpHeuristic/tmpMaxSpeed;
		}
		this.heuristic=tmpHeuristic;
	}
	
	@Override
	public double getHeuristic() {
		return this.heuristic;
	}
}
